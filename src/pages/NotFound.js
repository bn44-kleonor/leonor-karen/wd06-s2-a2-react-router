import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Section,
	Heading,
	Columns
} from 'react-bulma-components';

import TeamAdd from '../components/TeamAdd';
import TeamList from '../components/TeamList';


class TeamPage extends Component {
	render()
	{
		const sectionStyle = {
			paddingTop: "3em",
			paddingBottom: '3em'
		};

		return (
			<Section size="medium" style={ sectionStyle }>
				<p>Page not found</p>
			</Section>
		)
	}
}

export default TeamPage;
