import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Section,
	Heading,
	Columns
} from 'react-bulma-components';

import MemberAdd from '../components/MemberAdd';
import MemberList from '../components/MemberList';


class MemberPage extends Component {
	render()
	{
		const sectionStyle = {
			paddingTop: "3em",
			paddingBottom: '3em'
		};

		return (
			<Section size="medium" style={ sectionStyle }>
				<Heading>Members</Heading>
				<Columns>
					<Columns.Column size={4}>
						<MemberAdd />
					</Columns.Column>
					<Columns.Column>
						<MemberList />
					</Columns.Column>
				</Columns>
			</Section>
		)
	}
}

export default MemberPage;
