import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Card
} from 'react-bulma-components';

class TeamAdd extends Component {
	render()
	{
		return (
			<Card>
				<Card.Header>
					<Card.Header.Title>
						Add Team
					</Card.Header.Title>
				</Card.Header>
				<Card.Content>
					<form>
						<div className="field">
							<label className="label">Name</label>
							<div className="control">
								<input className="input" type="text" />
							</div>
						</div>
						<div className="field">
							<div className="control">
								<button className="button is-link is-success">Add</button>
							</div>
						</div>
					</form>
				</Card.Content>
			</Card>
		)
	}
}

export default TeamAdd;