import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Card
} from 'react-bulma-components';

class TaskAdd extends Component {
	render()
	{
		return (
			<Card>
				<Card.Header>
					<Card.Header.Title>
						Add Task
					</Card.Header.Title>
				</Card.Header>
				<Card.Content>
					<form>
						<div className="field">
							<label className="label">Task Description</label>
							<div className="control">
								<input className="input" type="text" />
							</div>
						</div>
						<div className="field">
						  <label className="label">Team</label>
						  <div className="control">
						    <div className="select is-fullwidth">
						      <select>
						        <option>Select team</option>
						        <option>With options</option>
						      </select>
						    </div>
						  </div>
						</div>
						<div className="field">
							<div className="control">
								<button className="button is-link is-success">Add</button>
							</div>
						</div>
					</form>
				</Card.Content>
			</Card>
		)
	}
}

export default TaskAdd;