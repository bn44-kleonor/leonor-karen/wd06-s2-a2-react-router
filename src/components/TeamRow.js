import React, { Component } from 'react';

class TeamRow extends Component {
	render ()
	{
		const team = this.props.x;
		return (
			<tr key={team._id}>
				<td>{team.name}</td>
				<td>{team.action}</td>
			</tr>
		)
	}
}

export default TeamRow;
