import React, { Component } from 'react';

class MemberRow extends Component {
	render ()
	{
		const member = this.props.x;
		return (
				<tr key={member._id}>
					<td>{member.firstName + " " + member.lastName}</td>
					<td>{member.position}</td>
					<td>{member.team}</td>
					<td>Sample</td>
				</tr>
		)
	}
}

export default MemberRow;
