import React, { Component } from 'react';

class TaskRow extends Component {
	render ()
	{
		const task = this.props.x;
		return (
			<tr key={task._id}>
				<td>{task.desc}</td>
				<td>{task.assignedTeam}</td>
				<td>{task.action}</td>
			</tr>
		)
	}
}

export default TaskRow;
