import React, { Component } from 'react';
import 'react-bulma-components/dist/react-bulma-components.min.css';
import {
	Card
} from 'react-bulma-components';
import TeamRow from './TeamRow';

class TeamList extends Component {
	state ={
		teams: []
	}

	componentDidMount()
	{
		let teamsCollection = [
			{
				_id: "teamid123a",
				name: "Instructor Team",
				action: " "
			},
			{
				_id: "teamid123b",
				name: "Tech Team",
				action: " "
			},
			{
				_id: "teamid123c",
				name: "HR Team",
				action: " "
			},
			{
				_id: "teamid123d",
				name: "N/A",
				action: " "
			},
			{
				_id: "teamid123e",
				name: "Others",
				action: " "
			}
		]
	

		this.setState({ teams: teamsCollection})

		console.log("Component mounted: ")
		console.log(this.state.teams)

	}

	componentDidUpdate()
	{
		console.log("Component updated: ")
		console.log(this.state.teams)
	}


	render()
	{
		return (
				<Card>
					<Card.Header>
						<Card.Header.Title>
							Team List
						</Card.Header.Title>
					</Card.Header>
					<Card.Content>
						<table className="table is-fullwidth is-bordered is-striped is-hoverable">
							<thead>
								<th>Team Name</th>
								<th>Action</th>
							</thead>
							<tbody>
							{
								this.state.teams.map(team => {
									return <TeamRow x={team}/>
								})
							}
							</tbody>
						</table>
					</Card.Content>
				</Card>
			)
	}
}

export default TeamList;